<?php


namespace Dracoder\Exceptions\Handlers;


use Exception;
use function var_dump;

class EchoExceptionHandler extends ExceptionHandler
{
    public function handle()
    {
        parent::do(function (Exception $exception)  {
            echo $exception->getMessage();
        }, [], $this->interruptsExecution);
    }
}