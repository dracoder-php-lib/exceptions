<?php

namespace Dracoder\Exceptions\Handlers;

use Exception;

/**
 * Class ExceptionHandler
 */
abstract class ExceptionHandler
{
    /** @var Exception $exception */
    protected Exception $exception;

    /** @var bool $interruptsExecution */
    protected bool $interruptsExecution;

    /**
     * ExceptionHandler constructor.
     *
     * @param Exception $exception
     * @param bool $interruptsExecution
     */
    public function __construct(Exception $exception, bool $interruptsExecution = true)
    {
        $this->exception = $exception;
        $this->interruptsExecution = $interruptsExecution;
    }

    /**
     * @return Exception
     */
    public function getException(): Exception
    {
        return $this->exception;
    }

    /**
     * @param Exception $exception
     */
    public function setException(Exception $exception): void
    {
        $this->exception = $exception;
    }

    /**
     * @return bool
     */
    public function isInterruptsExecution(): bool
    {
        return $this->interruptsExecution;
    }

    /**
     * @param bool $interruptsExecution
     */
    public function setInterruptsExecution(bool $interruptsExecution): void
    {
        $this->interruptsExecution = $interruptsExecution;
    }

    /**
     * @param callable $callback
     * @param array $parameters
     * @param bool $interruptsExecution
     */
    protected function do(callable $callback, array $parameters = [], bool $interruptsExecution = true)
    {
        $callback($this->exception, $parameters);
        if ($interruptsExecution) {
            die();
        }
    }

    abstract public function handle();
}