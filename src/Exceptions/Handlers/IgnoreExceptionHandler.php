<?php
namespace Dracoder\Exceptions\Handlers;

use Dracoder\Exceptions\Handlers\ExceptionHandler;
use Exception;

class IgnoreExceptionHandler extends ExceptionHandler
{
    /**
     * IgnoreExceptionHandler constructor.
     *
     * @param Exception $exception
     * @param bool $interruptExecution
     */
    public function __construct(Exception $exception, $interruptExecution = false)
    {
        parent::__construct($exception, $interruptExecution);
    }

    public function handle()
    {
        parent::do(function ($exception)  {
        }, [], $this->interruptsExecution);
    }
}