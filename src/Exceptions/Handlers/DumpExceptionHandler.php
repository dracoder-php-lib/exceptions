<?php
namespace Dracoder\Exceptions\Handlers;


class DumpExceptionHandler extends ExceptionHandler
{
    public function handle()
    {
        parent::do(function ($exception)  {
            var_dump($exception);
        }, [], $this->interruptsExecution);
    }
}