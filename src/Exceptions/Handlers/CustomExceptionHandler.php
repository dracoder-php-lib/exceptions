<?php
namespace Dracoder\Exceptions\Handlers;


use Exception;


class CustomExceptionHandler extends ExceptionHandler
{
    private $callback;

    private array $args;

    /**
     * CustomExceptionHandler constructor.
     *
     * @param Exception $exception
     * @param bool $interruptsExecution
     * @param $callback
     * @param array $args
     */
    public function __construct(Exception $exception, bool $interruptsExecution, $callback, array $args)
    {
        parent::__construct($exception, $interruptsExecution);
        $this->callback = $callback;
        $this->args = $args;
    }

    /**
     * @return mixed
     */
    public function getCallback()
    {
        return $this->callback;
    }

    /**
     * @param mixed $callback
     */
    public function setCallback($callback): void
    {
        $this->callback = $callback;
    }

    /**
     * @return array
     */
    public function getArgs(): array
    {
        return $this->args;
    }

    /**
     * @param array $args
     */
    public function setArgs(array $args): void
    {
        $this->args = $args;
    }



    public function handle()
    {
        parent::do($this->callback, $this->args, $this->interruptsExecution);
    }
}